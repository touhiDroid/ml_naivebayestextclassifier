package buet.touhiDroid.ml.utils;

/**
 * Created by touhid on 10/19/15.
 * @author touhid
 */
public class Utility {

    private static final String[] SKIP_LIST = {"i", "you", "he", "she",
            "am","is", "are", "was", "were", "be","been",
            "a", "the", "an", "with", "of", "at", "in", "on",
            "as", "so", "this", "that", "for", "to", "and", "but",
            "has", "have", "had", "no", "not", "it", "its", "from",
            "do", "does", "did", "must", "can", "could", "shall", "should", "will", "would",
            "may", "might", "before", "after", "since", "reuter"};

    public static boolean isWordSkippable(String word){
        for(String s : SKIP_LIST)
            if(word.equals(s))
                return true;
        return false;
    }
}
