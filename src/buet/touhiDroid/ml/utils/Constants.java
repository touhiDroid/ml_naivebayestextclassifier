package buet.touhiDroid.ml.utils;

/**
 * Created by touhid on 11/13/15.
 *
 * @author touhid
 */
public class Constants {

    public static final String FL_OUT = "out/knn_out";

    public static final String FL_TOPIC = "in/topics.data";
    public static final String FL_TRAIN = "in/training.data";
    public static final String FL_TEST = "in/test.data";
    public static final String FL_TRAIN_S = "in/test_mvr";
                                            //"in/training_s.data";
    public static final String FL_TEST_S = "in/test_mvr";
                                            //"in/test_sc.data";

    public static final String FL_TRAIN_MVR = "in/test_mvr";
    public static final String FL_TEST_MVR = "in/test_mvr";
}
