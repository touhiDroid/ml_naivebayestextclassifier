package buet.touhiDroid.ml.utils;

/**
 * Created by touhid on 4/22/15.
 *
 * @author touhid
 */
public class Lg {
    private static final boolean IS_APP_DEBUGGABLE = true;

    public static void p(String msg) {
        if (IS_APP_DEBUGGABLE)
            System.out.print(msg);
    }

    public static void pl(String msg) {
        if (IS_APP_DEBUGGABLE)
            System.out.println(msg);
    }

    public static void pl() {
        if (IS_APP_DEBUGGABLE)
            System.out.println();
    }

    public static void e(String msg, Exception e) {
        if (IS_APP_DEBUGGABLE) {
            System.out.println(msg);
            e.printStackTrace();
        }
    }
}
