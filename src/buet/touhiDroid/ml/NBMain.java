package buet.touhiDroid.ml;

import buet.touhiDroid.ml.models.TrainStructure;
import buet.touhiDroid.ml.utils.Constants;
import buet.touhiDroid.ml.utils.Lg;
import buet.touhiDroid.ml.utils.Utility;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by touhid on 11/14/15.
 *
 * @author touhid
 */
public class NBMain {

    private static final double LAP_SMOOTHING_PARAM = 1.0;


    // {list of [words] at the corresponding doc. index}
    private ArrayList<HashMap<String, Integer>> trainWordCountMapByTopicList;

    // {topic names for the corresponding doc. index}
    private ArrayList<String> trainDocTopicList;

    // {list of [words] at the corresponding doc. index}
    private ArrayList<Vector<String>> trainDocWordList;

    // <topicName, topicCount>
    private HashMap<String, Integer> topicCountMap;

    // <word, {doc. indices having this word} >
    private HashMap<String, ArrayList<Integer>> wordDocIndexMap;

    private HashMap<String, Double> topicProbability;
    private HashMap<String, Integer> topicWordCounter;

    // <topicName, <word, word-count> >
    private HashMap<String, HashMap<String, Integer>> topicWordCountMap;

    private int totalVocabSize;

    private int rightPredicts, wrongPredicts;

    private void readTrainData() {
        TrainStructure train = new TrainStructure();
        trainWordCountMapByTopicList = train.getWordCountMapByTopicList();
        trainDocWordList = train.getDocWordList();
        trainDocTopicList = train.getDocTopicList();
        topicCountMap = train.getTopicCountMap();
        topicWordCountMap = train.getTopicWordCountMap();
        wordDocIndexMap = train.getWordDocIndexMap();
    }

    private void calcNaiveBayesParams() {
        // TO_DO calc. all constant params from the train documents
        Lg.pl("Calculating topic probabilities ...");
        calcTopicProb();
        Lg.pl("Calculating topic-wise words...");
        countTopicWiseWords();
        totalVocabSize = wordDocIndexMap.size();

        Lg.pl("Computing probability of each word (in vocabulary) given any topic ...");
        computeVocabProbability();
    }

    // <word, <topicName, probability> >
    private HashMap<String, HashMap<String, Double>> wordForTopicProb;

    private void computeVocabProbability() {
        wordForTopicProb = new HashMap<String, HashMap<String, Double>>();
        // for every topic
        for (Map.Entry<String, HashMap<String, Integer>> twe : topicWordCountMap.entrySet()) {
            String topic = twe.getKey();

            double denom = topicWordCounter.get(topic) + totalVocabSize;

            // TODO for every word in the word-list : calc. prob given this topic

            for (String word : wordDocIndexMap.keySet()) {
                Integer wc = twe.getValue().get(word);
                if (wc == null) wc = 0; // this topic doesn't have this word.

                double prob = (wc + LAP_SMOOTHING_PARAM) / denom;
                HashMap<String, Double> wtProbMap = wordForTopicProb.get(word);
                if (wtProbMap == null) {
                    wtProbMap = new HashMap<String, Double>();
                    wtProbMap.put(topic, prob);
                    wordForTopicProb.put(word, wtProbMap);
                } else
                    wtProbMap.put(topic, prob);
            }

            /*for (Map.Entry<String, Integer> we : twe.getValue().entrySet()) {
                double prob = (we.getValue() + LAP_SMOOTHING_PARAM) / denom;
                String word = we.getKey();

                HashMap<String, Double> wtProbMap = wordForTopicProb.get(word);
                if (wtProbMap == null) {
                    wtProbMap = new HashMap<String, Double>();
                    wtProbMap.put(topic, prob);
                    wordForTopicProb.put(word, wtProbMap);
                } else
                    wtProbMap.put(topic, prob);
            }*/
        }
    }

    private void calcTopicProb() {
        topicProbability = new HashMap<String, Double>();
        double sz = (double) trainDocTopicList.size();
        for (Map.Entry<String, Integer> e : topicCountMap.entrySet()) {
            String topic = e.getKey();
            double tc = (double) e.getValue();
            if (topicProbability.containsKey(topic))
                continue;
            topicProbability.put(topic, (tc / sz));
        }
    }

    private void countTopicWiseWords() {
        // TO_DO Count words of each topic and push to a hashmap by that Topic-Name
        topicWordCounter = new HashMap<String, Integer>();
        int totalTopicCount = trainDocTopicList.size();
        for (int i = 0; i < totalTopicCount; i++) {
            String topic = trainDocTopicList.get(i);
            int wtc = trainDocWordList.get(i).size(); // how many words in total under this document?

            Integer prev = topicWordCounter.get(topic); // has any prev. word-count for this topic?
            if (prev == null)
                topicWordCounter.put(topic, wtc);
            else
                topicWordCounter.put(topic, wtc + prev);
        }
    }

    private void readAndPredictTestData() {
        // TO_DO Read each test item
        // TODO predict by Naive-Bayes method (probability calc.)
        // TODO with Laplace Smoothing (diff. smoothing params)
        // => from testing() to the end
        rightPredicts = wrongPredicts = 0;

        HashMap<String, Integer> wordCountMap = new HashMap<String, Integer>();
        String curTopic = "";

        int prevNewLine = 0;
        int lineCount = 0;
        try {
            Scanner testScanner = new Scanner(new File(Constants.FL_TEST));
            while (testScanner.hasNextLine()) {
                String curLine = testScanner.nextLine();
                lineCount++;

                if (curLine.length() == 0) {
                    prevNewLine++; // just an empty new line
                    continue;
                } else {
                    if (prevNewLine > 1) {
                        // TO_DO predict previous topic
                        Lg.p("Predicting topic : original = " + curTopic + " ... ");
                        // predictTopic(curTopic, wordCountMap);
                        predictAndCompareTopic(curTopic, wordCountMap);

                        wordCountMap.clear();
                        curTopic = curLine; // setting new topic
                        prevNewLine = 0;
                        continue;
                    } else
                        prevNewLine = 0; // let's parse this non-header "long" line with "words"
                }
                if (lineCount == 1) { // 1st line - so direct new topic without any preceding new-line
                    curTopic = curLine;
                } else if (curLine.length() != 0) {
                    String[] lineWords = curLine.replaceAll("\\p{P}", "").toLowerCase().split("\\s+");

                    for (String word : lineWords) {
                        if (Utility.isWordSkippable(word))
                            continue;
                        Integer pwc = wordCountMap.get(word);
                        if (pwc != null)
                            wordCountMap.put(word, pwc + 1);
                        else
                            wordCountMap.put(word, 1);
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Lg.pl("Test :: DocWordList Size: " + wordCountMap.size());
    }

    private void predictAndCompareTopic(String origTopic, HashMap<String, Integer> wordCountMap) {
        double maxProb = Long.MIN_VALUE;
        String predictedTopic = "";

        // for every topic
        for (String topic : topicWordCounter.keySet()) {

            Double prob = topicProbability.get(topic);
            if (prob == null) prob = 0.0; // new topic from this test data, wasn't in the train-samples
            else prob = (Math.log(prob));//Math.abs

            // TODO
            // for every word in the provided document :
            // take sum of each word's prob.'s LOG + topic_probability
            for (String word : wordCountMap.keySet()) {
                HashMap<String, Double> wordProbMap = wordForTopicProb.get(word);

                if (wordProbMap == null) prob += 1; // New word in this test,
                else {
                    Double p = wordProbMap.get(topic);
                    if (p == null) p = 0.0; // this word has no prob. for this topic - r u kidding me? :\
                    prob += (Math.log(p));//Math.abs
                }
            }

            if(prob>maxProb){
                maxProb = prob;
                predictedTopic = topic;
            }

        }
        Lg.p("predicted = " + predictedTopic + "  :: ");
        if (predictedTopic.equals(origTopic)) {
            rightPredicts++;
            Lg.pl(" Right :)");
        }
        else {
            wrongPredicts++;
            Lg.pl(" Wrong :(");
        }
    }

    private void predictTopic(String origTopic, HashMap<String, Integer> wordCountMap) {
        String predictedTopic = "";
        double maxProb = Double.MIN_VALUE;
        for (Map.Entry<String, Double> e : topicProbability.entrySet()) {
            String topicName = e.getKey();
            double topicProb = e.getValue();

            int n = topicWordCounter.get(topicName);
            // TO_DO Loop to get all word prob.
            // ei topic-er under-e 1ta doc-er kno specific word kotobar ache,
            // seta die prob. ber kore log nie sum kore max. korte hbe
            double p = 0.0;
            for (int i = 0; i < trainDocTopicList.size(); i++) {
                if (trainDocTopicList.get(i).equals(topicName)) {
                    for (Map.Entry<String, Integer> we : wordCountMap.entrySet()) {
                        Integer nk = trainWordCountMapByTopicList.get(i).get(we.getKey());
                        if (nk == null) nk = 0;
                        p += Math.abs(Math.log((nk + .03) / (n + totalVocabSize)) * topicProb);
                    }
                }
            }

            // TODO Compare the calculated prob. with the max prob. & update the prediction if necessary
            if (p > maxProb) {
                maxProb = p;
                predictedTopic = topicName;
            }
        }
        // TO_DO Print the prediction as Lg.pl() & keep count of the right/wrong predictions
        if (predictedTopic.equals(origTopic))
            rightPredicts++;
        else
            wrongPredicts++;
        Lg.pl(predictedTopic + "  :: (r=" + rightPredicts + " / " + "w=" + wrongPredicts + ")");
    }

    public NBMain() {
        readTrainData();
        calcNaiveBayesParams();
        Lg.pl();    Lg.pl();
        readAndPredictTestData();

        Lg.pl();
        double accuracy = ((double)(rightPredicts)) / ((double)(rightPredicts+wrongPredicts)) * 100.0;
        Lg.pl("Accuracy : "  + accuracy);
        Lg.pl("Laplace smoothing parameter = " + LAP_SMOOTHING_PARAM);
    }

    public static void main(String[] args) {
        new NBMain();
    }
}
