package buet.touhiDroid.ml.models;

import buet.touhiDroid.ml.utils.Constants;
import buet.touhiDroid.ml.utils.Lg;
import buet.touhiDroid.ml.utils.Utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

/**
 * Created by touhid on 11/4/15.
 *
 * @author touhid
 */
public class TrainStructure {

    // {list of [words] at the corresponding doc. index}
    private ArrayList<HashMap<String, Integer>> wordCountMapByTopicList = new ArrayList<HashMap<String, Integer>>();

    // {list of [words] at the corresponding doc. index}
    private ArrayList<Vector<String>> docWordList = new ArrayList<Vector<String>>();

    // {topic names for the corresponding doc. index}
    private ArrayList<String> docTopicList = new ArrayList<String>();

    // <topicName, <word, word-count> >
    private HashMap<String, HashMap<String, Integer>> topicWordCountMap
            = new HashMap<String, HashMap<String, Integer>>();

    // <topicName, topicCount>
    private HashMap<String, Integer> topicCountMap = new HashMap<String, Integer>();

    // <word, {doc. indices having this word} >
    private HashMap<String, ArrayList<Integer>> wordDocIndexMap = new HashMap<String, ArrayList<Integer>>();

    private File trainFile;

    public TrainStructure() {
        trainFile = new File(Constants.FL_TRAIN);  //_S
        Lg.p("Reading train file ...");
        getCountsFromFile();
        readInputFile();
        Lg.pl("=> train file reading done!");
    }

    private void getCountsFromFile() {
        int prevNewLine = 0;
        int count = 0;
        try {
            Scanner trScnr = new Scanner(trainFile);
            while (trScnr.hasNextLine()) {
                String curLine = trScnr.nextLine();
                if (curLine.length() == 0) {
                    prevNewLine++;
                } else {
                    if (prevNewLine > 1) {
                        docTopicList.add(curLine);
                        Integer tc = topicCountMap.get(curLine);
                        if (!topicWordCountMap.containsKey(curLine))
                            topicWordCountMap.put(curLine, new HashMap<String, Integer>());
                        if (tc != null)
                            topicCountMap.put(curLine, tc + 1);
                        else
                            topicCountMap.put(curLine, 1);
                    }
                    prevNewLine = 0;
                }
                if (count == 0) {
                    docTopicList.add(curLine);
                    Integer tc = topicCountMap.get(curLine);
                    if (tc != null)
                        topicCountMap.put(curLine, tc + 1);
                    else
                        topicCountMap.put(curLine, 1);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readInputFile() {
        int prevNewLine = 0;
        int lineCount = 0;
        int docIndex = 0;
        String curTopic = "";
        try {
            Scanner trScnr = new Scanner(trainFile);
            while (trScnr.hasNextLine()) {
                String curLine = trScnr.nextLine();
                lineCount++;
                if (curLine.length() == 0) {
                    prevNewLine++;
                    continue;
                } else {
                    if (prevNewLine > 1) {
                        docIndex++;
                        curTopic = curLine;
                        docWordList.add(new Vector<String>());
                        wordCountMapByTopicList.add(new HashMap<String, Integer>());
                        if (!topicWordCountMap.containsKey(curLine))
                            topicWordCountMap.put(curLine, new HashMap<String, Integer>());
                        prevNewLine = 0;
                        // Lg.p(".");
                        continue;
                    }
                    prevNewLine = 0;
                }
                if (lineCount == 1) {
                    docIndex = 0;
                    curTopic = curLine;
                    wordCountMapByTopicList.add(new HashMap<String, Integer>());
                    docWordList.add(new Vector<String>());
                } else if (curLine.length() != 0) {
                    String[] lineWords = curLine.replaceAll("\\p{P}", "").toLowerCase().split("\\s+");

                    for (String word : lineWords) {
                        if (Utility.isWordSkippable(word))
                            continue;
                        HashMap<String, Integer> hash = wordCountMapByTopicList.get(docIndex);
                        Integer val = hash.get(word);
                        docWordList.get(docIndex).add(word);
                        if (val != null) {
                            hash.put(word, val + 1);
                        } else {
                            hash.put(word, 1);
                        }
                        if (wordDocIndexMap.containsKey(word)) {
                            if (!wordDocIndexMap.get(word).contains(docIndex)) {
                                wordDocIndexMap.get(word).add(docIndex);
                            }

                        } else {
                            wordDocIndexMap.put(word, new ArrayList<Integer>());
                            wordDocIndexMap.get(word).add(docIndex);
                        }
                        HashMap<String, Integer> wmp = topicWordCountMap.get(curTopic);
                        Integer pc = wmp.get(word);
                        if (pc == null) pc = 0;
                        wmp.put(word, pc + 1);
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Lg.pl("\nTotal train-documents: " + docTopicList.size());
        Lg.pl("Train :: DocWordList Size: " + docWordList.size()
                + ",  Word-Map size: " + wordDocIndexMap.size());
    }

    public ArrayList<HashMap<String, Integer>> getWordCountMapByTopicList() {
        return wordCountMapByTopicList;
    }

    public ArrayList<Vector<String>> getDocWordList() {
        return docWordList;
    }

    public ArrayList<String> getDocTopicList() {
        return docTopicList;
    }

    public HashMap<String, Integer> getTopicCountMap() {
        return topicCountMap;
    }

    public HashMap<String, ArrayList<Integer>> getWordDocIndexMap() {
        return wordDocIndexMap;
    }

    public HashMap<String, HashMap<String, Integer>> getTopicWordCountMap() {
        return topicWordCountMap;
    }
}
