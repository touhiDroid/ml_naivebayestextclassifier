package buet.touhiDroid.ml.models;
/**
 * Created by touhid on 11/4/15.
 *
 * @author touhid
 */
public class DistanceHolder {

    private String topicName;
    private int hammingDist;
    private int euclidianDist;
    private double cosineSimilarity;


    public DistanceHolder(String topicName, int hammingDist, int euclidianDist,
                          double cosineSimilarity) {
        this.topicName = topicName;
        this.hammingDist = hammingDist;
        this.euclidianDist = euclidianDist;
        this.cosineSimilarity = cosineSimilarity;
    }

    public double getCosineSimilarity() {
        return cosineSimilarity;
    }

    public void setCosineSimilarity(double cosineSimilarity) {
        this.cosineSimilarity = cosineSimilarity;
    }

    public int getHammingDist() {
        return hammingDist;
    }

    public void setHammingDist(int hammingDist) {
        this.hammingDist = hammingDist;
    }

    public int getEuclidianDist() {
        return euclidianDist;
    }

    public void setEuclidianDist(int euclidianDist) {
        this.euclidianDist = euclidianDist;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

}
